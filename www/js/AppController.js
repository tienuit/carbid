﻿window.fn = {};

window.fn.open = function () {
    var menu = document.getElementById('menu');
    menu.open();
};
window.fn.close = function () {
    var menu = document.getElementById('menu');
    menu.close();
};
window.fn.load = function (page) {
    var content = document.getElementById('content');
    var menu = document.getElementById('menu');
    content.load(page)
        .then(menu.close.bind(menu));
};

var server = "http://c24h.zeusgang.com"

var module = ons.bootstrap('app', ['onsen', 'ngSanitize']);
//angular.module('app', ['onsen', 'ngSanitize']);

angular.module('app').controller('AppController', function ($scope, $rootScope, $http, $sce) {
    
    $rootScope.isLogged = false;
    $rootScope.isSeller = false;

    //localStorage.User = JSON.stringify({ UserName: "Test", Roles: ["seller"], LoginToken: "cae5b39e-055d-4d0c-8433-0e39b846aaa7" });
    if (localStorage.User != null && localStorage.User != "null") {
        $rootScope.isLogged = true;
        $rootScope.user = JSON.parse(localStorage.User);
        $rootScope.isSeller = $rootScope.user.Roles.indexOf('seller') >= 0;

        console.log($rootScope.user.LoginToken);
	}

    $http.get(server + "/api/Categories/List").then(function (response) {
        $scope.categories = response.data;
    });

    $http.get(server + "/api/Brands/GetAll").then(function (response) {
        $scope.brands = response.data;
    });

    $http.get(server + "/api/Cars/SeatNos").then(function (response) {
        $scope.seatNos = response.data;
    });

    $scope.goToCarDetails = function (car) {
        //var options = {
        //    //animation: 'slide', // What animation to use
        //    onTransitionEnd: function () { }, // Called when finishing transition animation
        //};

        myNavigator.pushPage("cardetails.html");
        $scope.currentCar = car;

    };

    $scope.goToBrandDetails = function (brand)
    {
        var options = {
            //animation: 'slide', // What animation to use
            onTransitionEnd: function () { }, // Called when finishing transition animation
        };

        $scope.currentBrand = brand;
        myNavigator.pushPage("branddetails.html", options);
       
    }

    $scope.goToCategoryDetails = function (category) {
        console.log('goToCategoryDetails', category.Name);
        var options = {
            //animation: 'slide', // What animation to use
            onTransitionEnd: function () { }, // Called when finishing transition animation
        };
        $scope.currentCategory = category;
        myNavigator.pushPage("categorydetails.html", options);
        //$http.get(server + "/api/Cars/ByCategory?categoryId=" + category.Id).then(function (response) {
        //    $scope.currentCategory.Cars = response.data;
        //    myNavigator.pushPage("categorydetails.html", options);
        //});
        
    }
    
    $scope.logout = function () {
        $rootScope.isLogged = false;
        $rootScope.user = null;
        localStorage.User = null;
        console.log("log out");
    }
	
});

angular.module('app').controller('HomePageController', function ($scope, $rootScope, $http) {
   
});

angular.module('app').controller('AllCarsPageController', function ($scope, $rootScope, $http) {
    $scope.categoryFilterValue = "0";
    $scope.brandFilterValue = "0";
    $scope.priceFilterValue = "0-30000";
    $scope.seatNoFilterValue = "-1";

    $scope.page = 1;
    $scope.loadData = function () {
        var url = server + "/api/Cars/GetMany?page=" + $scope.page;
        if ($rootScope.user != null)
            url += "&loginToken=" + $rootScope.user.LoginToken;
        console.log(url);
        $http.get(url).then(function (response) {
            $scope.cars = response.data;
        });
    };
    $scope.loadMore = function (done) {
        console.log("loadmore");
        $scope.page++;
        var url = server + "/api/Cars/GetMany?categoryId=" + $scope.categoryFilterValue + "&brandId=" + $scope.brandFilterValue + "&seatNo=" + $scope.seatNoFilterValue + "&price=" + $scope.priceFilterValue +  "&page=" + $scope.page;
        if ($rootScope.user != null)
            url += "&loginToken=" + $rootScope.user.LoginToken;
        $http.get(url).then(function (response) {
            $scope.cars = $scope.cars.concat(response.data);
            done();
        });
    }
    $scope.popupFilter = function () {
        $scope.showDialog = !$scope.showDialog;
    };

    $scope.filterCars = function ()
    {
        $scope.page = 1;
        var url = server + "/api/Cars/GetMany?categoryId=" + $scope.categoryFilterValue + "&brandId=" + $scope.brandFilterValue + "&seatNo=" + $scope.seatNoFilterValue + "&price=" + $scope.priceFilterValue + "&page=" + $scope.page;
        if ($rootScope.user != null)
            url += "&loginToken=" + $rootScope.user.LoginToken;
        console.log(url);
        $http.get(url).then(function (response) {
            $scope.cars = response.data;
        });
        $scope.showDialog = false;
    }
});

angular.module('app').controller('RequestListPageController', function ($scope, $rootScope, $http) {
    var url = server + "/api/UserRequests/UnansweredList?loginToken=" + $rootScope.user.LoginToken;
    var url2 = server + "/api/UserRequests/AnsweredList?loginToken=" + $rootScope.user.LoginToken;
    
    var loadData = function () {
        $http.get(url).then(function (response) {
            $scope.unansweredRequests = response.data;
        });
        $http.get(url2).then(function (response) {
            $scope.answeredRequests = response.data;
        });
    }

    loadData();

    $scope.goToRequestAnswer = function (request) {
        //console.log(request);
        var options = {
            data:
            {
                request: request
            }
        }
        myNavigator.pushPage("sendrequestanswer.html", options);
    };
});

angular.module('app').controller("SendRequestAnswerController", function ($scope, $rootScope, $http) {
    $scope.sendAnswer = function (request)
    {
        var data = {
                Reply: $scope.reply,
                BidPrice: $scope.bidprice,
                BuyerId: request.UserId,
                CarId: request.CarId,
                LoginToken: $rootScope.user.LoginToken
        };
        $http.post('http://bidcar.azurewebsites.net/api/UserRequests/Answer', data)
            .then(function (response) {
                var data = response.data;
                console.log(data);
                if (data == true)
                {
                    myNavigator.popPage();
                }
            },
            function () {
                console.log('error');
            });
    }
});

angular.module('app').controller("SendPriceRequestController", function ($scope, $rootScope, $http) {

    $scope.sendRequest = function (car) {
        if ($rootScope.isLogged == true) {
            var data = {
                userId: $rootScope.user.UserId,
                loginToken: $rootScope.user.LoginToken,
                carId: car.Id,
                content: $scope.content
            };
            $http.post('http://bidcar.azurewebsites.net/api/Users/SendPriceRequest', data)
                .then(function (response) {
                    var data = response.data;
                    console.log(data);
                    car.IsRequested = true;
                    myNavigator.popPage();
                },
                function () {
                    console.log('error');
                });
        }
        else
        {
            myNavigator.pushPage("login.html");
        }
    }
});

angular.module('app').controller("PriceRequestDetailsController", function ($scope, $rootScope, $http) {
    $scope.call = function (phoneNumber)
    {
        console.log('call:' + phoneNumber);
        document.location.href = "tel:" + phoneNumber;
    }
});
angular.module('app').controller('BrandsPageController', function ($scope, $rootScope, $http) {
   
    
});

angular.module('app').controller('CategoriesPageController', function ($scope, $http) {
    
    
});


angular.module('app').controller('SeatNosPageController', function ($scope, $http) {
    
    $scope.goToSeatNoDetails = function (seatNoValue)
    {
        var options = {
            data:
            {
                seatNo: {
                    Value : seatNoValue
                }
            }
        }
        myNavigator.pushPage("seatNoDetails.html", options);   
    }
});

angular.module('app').controller('CategoryDetailsPageController', function ($scope, $http)
{
    $scope.page = 1;
    $http.get(server + "/api/Cars/ByCategory?categoryId=" + $scope.currentCategory.Id).then(function (response) {
            $scope.currentCategory.Cars = response.data;
    });

    //$scope.loadMore = function (done) {
    //    console.log("loadmore");
    //    $scope.page++;
    //    $http.get(server + "/api/Cars/ByCategory?categoryId=" + $scope.currentCategory.Id + "&page=" + $scope.page).then(function (response) {
    //        $scope.currentCategory.Cars = $scope.currentCategory.Cars.concat(response.data);
    //        done();
    //    });
    //}
});
angular.module('app').controller('BrandDetailsPageController', function ($scope, $http) {

    $http.get(server + "/api/Cars/ByBrand?brandId=" + $scope.currentBrand.Id).then(function (response) {
        $scope.currentBrand.Cars = response.data;
    });
});

angular.module('app').controller('SeatNoDetailsPageController', function ($scope, $http) {
    $scope.init = function () {
        console.log("init");
        $scope.seatNo = myNavigator.topPage.data.seatNo; // Will return "value1"    
        $http.get(server + "/api/Cars/GetMany?seatNo=" + $scope.seatNo.Value).then(function (response) {
            $scope.seatNo.Cars = response.data;
        });
    }
});

angular.module('app').controller('LoginPageController', function ($scope, $rootScope, $http) {
    $scope.login = function () {

        //alert("Login");
        //alert(device.uuid);
        var data = {
            username: $scope.username,
            password: $scope.password,
            deviceId: device.uuid
        };
        console.log($scope.username);
        console.log($scope.password);
        alert(data.deviceId);
        $http({
            method: 'POST',
            url: server + "/api/Users/Login",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function (obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: data
        })
        .then(function success(response) {
                var data = response.data;
            console.log(data.Success);
            if (data.Success == "true") {
                localStorage.User = JSON.stringify(data.UserInfo);
                $rootScope.isLogged = true;
                $rootScope.user = data.UserInfo;
                $rootScope.isSeller = $rootScope.user.Roles.indexOf('seller') >= 0;
                console.log("true");
                fn.load('home.html');
            }
            else {
                console.log("false");
            }
            }, function error(response) {
                console.log("error");
        });
    }
});
// module('app').controller('MenuPageController', function ($scope, $http) {
   
// });
angular.module('app').controller('CarDetailsPageController', function ($scope, $rootScope, $http)
{
    $http.get(server + "/api/Cars/GetAllPlace").then(function (response) {
        $scope.places = response.data;
        $scope.place = $scope.places[0];
        $scope.updateStampTax();
    });
    $scope.gotoSendPriceRequest = function (car)
    {
        if ($rootScope.isLogged) {
            var options = {
                data:
                {
                    car: car
                }
            }
            myNavigator.pushPage("sendpricerequest.html", options);
        }
        else
        {
            myNavigator.pushPage("login.html");
        }
    }
    $scope.gotoPriceRequestDetails = function (car) {
        var options = {
            data:
            {
                car: car
            }
        }
        myNavigator.pushPage("pricerequestdetails.html", options);

    }
    $scope.updateStampTax = function ()
    {
        if ($scope.currentCar.Type == 'Pick-up') {
            $scope.stampTaxPercent = 2;
        } else {
            $scope.stampTaxPercent = $scope.place.Id == 1 ? 12 : 10;
        }
    }

});

